// global stylesheets, colors, dimensions etc.
export default {
    colors: {
        main: 'white',
        pink: 'pink',
        red: 'red'
    },
    columns: {

    },
    structure: {

    },
    typography: {
      main_size: 16
    }
};
