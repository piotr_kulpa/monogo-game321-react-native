import React from 'react';
import { StyleSheet, Text, View, Image, ImageBackground, Button, TouchableHighlight } from 'react-native';
import { withClientState } from 'apollo-link-state';
import FontAwesome, { Icons } from 'react-native-fontawesome';
import Footer from '../components/Footer';
import {ApolloProvider} from "react-apollo";

export default class SignIn extends React.Component {


    showAllert() {
      alert('ok');

    }

    render() {



        return (
            <ImageBackground source={require('../assets/images/sign_in.jpg')} style={{width: '100%', height: '100%'}} >


                <View style={styles.container}>


                    <Text style={styles.welcome}>Welcome to</Text>


                    <View style={styles.logo}>
                        <Image source={require('../assets/images/logo.png')} style={{width: '100%', height: '100%'}} />
                    </View>


                    <Text style={styles.siginsigninTxtStart}>
                        <Text style={styles.siginsigninBold}>Sign in </Text>
                        to get start
                    </Text>
                    <TouchableHighlight
                        style={styles.submit}
                        onPress={() => this.props.navigation.navigate('SignIn')}
                        underlayColor='#fff'>
                        <Text style={[styles.submitText]}>SIGN IN</Text>
                    </TouchableHighlight>
                    <Text style={styles.siginsigninTxt}>By signing up you agree to the</Text>
                    <Text style={styles.siginsigninTxtPolicy} >Terms od Use Privacy Policy and Rules</Text>
                </View>

                <Footer></Footer>


            </ImageBackground>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        marginTop: 50,
        alignItems: 'center',
        backgroundColor: 'rgba(0, 0, 0, 0)'
    },
    welcome: {
        textAlign: 'center',
        fontSize: 22,
        color: 'white'

    },
    bckImage: {
        width: '100%'
    },
    logo: {
        width: 200,
        height: 200,
        backgroundColor: 'rgba(0, 0, 0, 0)',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 35

    },
    siginBtn: {
        backgroundColor: 'rgba(90, 17, 67, 0)',
        marginTop: 135
    },
    siginsigninTxt: {
        color: '#fff',
        marginTop: 35
    },
    siginsigninTxtPolicy: {
        color: '#fff',
        marginTop: 0,
        fontWeight: 'bold'
    },
    siginsigninTxtStart: {
        color: '#fff',
        marginTop: 20,
        marginBottom: 10,
        fontSize: 25
    },
    siginsigninBold: {
        color: '#fff',
        fontSize: 25,
        fontWeight: 'bold'
    },
    submit:{
        marginRight:40,
        marginLeft:40,
        marginTop:10,
        paddingTop:10,
        paddingBottom:10,
        backgroundColor:'#e72dac',
        borderRadius:20,
        borderWidth: 0,
        width: 150
    },
    submitText:{
        color:'#fff',
        textAlign:'center',
    }
});
