import React, {Component} from 'react';
import { Text, View, StyleSheet} from 'react-native';
import Container from '../components/Container';

export default class Profile extends Component {
  render() {
    return (
      <Container>
        <View style={styles.container}>
          <Text>
          Profile screen
          </Text>
        </View>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0)'
  }
});
