import React, { Component } from 'react';
import { AppRegistry, FlatList, StyleSheet, Text, View } from 'react-native';
import {withApollo} from "react-apollo";
import gql from 'graphql-tag';
import {graphql} from "react-apollo";
import Container from '../components/Container';

const testQuery = gql`
{
  JtiHeadlessCatalogProductResults(categoryId:2, storeId:1) {
    items {
      name
      entity_id
    }
  }
}
`;

class FlatListBasics extends Component {

    state = {
      loading: '',
      error: '',
      data: {}
    }

    componentDidMount() {
      //this.checkGqlData();
    }

    checkGqlData() {
      const {data} = this.props;
      const {loading, error, JtiHeadlessCatalogProductResults} = data || {};

      if (loading) {
        this.setState({loading: 'Loading'})
      }

      if (error) {
        this.setState({loading: '', error: 'Error!'})
      }

      const {items} = JtiHeadlessCatalogProductResults;
    }

    render() {


        return (
          <Container>
            <Text>loading</Text>
            {/*
            <View style={styles.container}>
                <Text>{items.map((item, index) => <Text key={index}>{item.name}</Text>)}</Text>
            </View>
            */}
          </Container>
        );
    }
}

export default graphql(testQuery)(withApollo(FlatListBasics));

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 22
    },
    item: {
        padding: 10,
        fontSize: 18,
        height: 44,
    },
})

// skip this line if using Create React Native App
//AppRegistry.registerComponent('AwesomeProject', () => FlatListBasics);
