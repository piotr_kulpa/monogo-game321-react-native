import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { connect } from 'react-redux';
import GS from '../stylesheet/';

class Contact extends React.Component {
  render() {
    alert(this.props.loader)
    return (
      <View>
        <View style={styles.container}>
          <Text>Contact component {this.props.loader}</Text>
        </View>
        <View style={styles.container2}>
          <Text>New line</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {

    backgroundColor: GS.colors.main,
    alignItems: 'center',
    justifyContent: 'center',
  },
  container2: {
    backgroundColor: GS.colors.red,
    color: 'red'
  }
});

const mapStateToProps = state => {
  console.log(state);
  return {
    loader: state.showLoader
  };
}

export default connect(mapStateToProps)(Contact);
