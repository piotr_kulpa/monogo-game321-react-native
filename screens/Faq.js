import React, {Component} from 'react';
import { Text, View, StyleSheet, Button, TouchableHighlight, ScrollView } from 'react-native';
import Container from '../components/Container'
import Accordion from 'react-native-collapsible/Accordion';
import { Ionicons } from '@expo/vector-icons';
import faqQuery from '../queries/faqQuery.js';

import {withApollo} from "react-apollo";
import gql from 'graphql-tag';
import {graphql} from "react-apollo";

class Faq extends Component {

  state = {
    activeSections: [],
  };

  _renderSectionTitle = section => {
    return (

      <View style={styles.content}>
        <Text>{section.header}</Text>
      </View>

    );
  };

  _renderHeader = (section, i) => {

    const backgroundColors = ['#02bfb9', '#ff0078', '#a200d0', '#00a8ff', '#02bfb9', '#ff0078'];

    return (

          <View style={{borderRadius: 15, flex: 1, flexDirection: 'row', justifyContent: 'space-between', backgroundColor: backgroundColors[i]}}>
            <Text style={styles.faqBar}>
            {section.header}
            </Text>
            <Ionicons name="md-arrow-dropdown" size={22} color="white" />

          </View>

    );
  };

  _renderContent = section => {
    return (
      <View style={styles.faqContent}>
        <Text style={styles.faqContent}>{section.text}</Text>
      </View>
    );
  };

  _updateSections = activeSections => {
    this.setState({ activeSections });
  };

    render() {

      const { loading, error, Game321QuizGameApiStaticContentGetStaticContent } = this.props.data || {};
      //const {faq} = Game321QuizGameApiStaticContentGetStaticContent || {};
      if (loading) {
        return (
          <Text>Loading...</Text>
        )
      }

      if (error) {
        return <Text>Error</Text>
      }

      const {faq} = Game321QuizGameApiStaticContentGetStaticContent;


        return (
          <Container>
            <Text style={styles.header}>{faq.title}</Text>

            <View style={styles.faqContainer}>
            <ScrollView>
              <Accordion
                sections={faq.content}
                activeSections={this.state.activeSections}
                renderHeader={this._renderHeader}
                renderContent={this._renderContent}
                onChange={this._updateSections}
              />
              </ScrollView>
            </View>
          </Container>
        );
    }
}

const styles = StyleSheet.create({
  faqContainer: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0)',
    marginLeft: 20,
    marginRight: 20
  },
  faqBar: {

    fontSize: 22,
    color: 'white',
    padding: 10,
    fontWeight: 'bold',
    marginTop: 5
  },
  header: {
    textAlign: 'center',
    fontSize: 22,
    color: 'white',
    margin: 0
  },
  faqH1: {
    color: 'white',
    fontSize: 18,
    paddingTop: 5,
    paddingBottom: 5,
    fontWeight: 'bold'
  },
  faqH2: {
    color: '#02e4d8',
    fontSize: 12,
    fontWeight: 'bold'
  },
  faqContent: {
    color: 'white',
    padding: 5
  },
  faqBarColor0: {
    backgroundColor: '#02bfb9'
  },
  faqBarColor1: {
    backgroundColor: '#ff0078'
  },
  faqBarColor2: {
    backgroundColor: '#a200d0'
  },
  faqBarColor3: {
    backgroundColor: '#00a8ff'
  },
  faqBarColor4: {
    backgroundColor: '#02bfb9'
  },
  faqBarColor5: {
    backgroundColor: '#ff0078'
  }
});

export default graphql(faqQuery)(withApollo(Faq));
