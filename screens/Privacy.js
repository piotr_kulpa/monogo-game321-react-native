import React, {Component} from 'react';
import { Text, View, StyleSheet, FlatList } from 'react-native';
import Container from '../components/Container';
//apollo
import {withApollo} from "react-apollo";
import gql from 'graphql-tag';
import {graphql} from "react-apollo";

import privacyQuery from '../queries/privacyQuery.js'

class Privacy extends Component {


  render() {


    const {data} = this.props;
    const {loading, error, Game321QuizGameApiStaticContentGetStaticContent} = data || {};

    console.log(Game321QuizGameApiStaticContentGetStaticContent);

    if (loading) {
      return (
        <Text>Loading...</Text>
      )
    }

    if (error) {
      return <Text>Error</Text>
    }

    const {privacy} = Game321QuizGameApiStaticContentGetStaticContent;

    return (
      <Container>

      <Text style={styles.header}>{privacy.title}</Text>
        <FlatList
          style={styles.container}
          data={privacy.content}
          renderItem={({item}) => {
            return (
            <View>
              <Text style={styles.privacyH1}>{item.header}</Text>
              <Text style={styles.privacyH2}>{item.subheader}</Text>
              <Text style={styles.privacyContent}>{item.text}</Text>
            </View>
            )
          }}
        />
      </Container>
    );
  }
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0)',
    marginLeft: 20,
    marginRight: 20,
    color: 'white'

  },
  header: {
    textAlign: 'center',
    fontSize: 22,
    color: 'white',
    margin: 0
  },
  privacyH1: {
    color: 'white',
    fontSize: 18,
    paddingTop: 15,
    paddingBottom: 15,
    fontWeight: 'bold'
  },
  privacyH2: {
    color: '#02e4d8',
    fontSize: 12,
    fontWeight: 'bold'
  },
  privacyContent: {
    color: 'white'
  }
});

export default graphql(privacyQuery)(withApollo(Privacy));
//export default Privacy;
