import React, { Component } from 'react';
import { Button, CameraRoll, Image, Text, View, StyleSheet, PermissionsAndroid } from 'react-native';
import { ImagePicker, Constants, takeSnapshotAsync, Permissions } from 'expo';

export default class TestImagePicker extends Component {
  state = {
    cameraRollUri: null,
    image: null
  };

  _checkPermissions = async () => {
    const { status, expires, permissions } = await Permissions.getAsync(Expo.Permissions.CAMERA_ROLL)
    if (status !== 'granted') {
      //tutaj logika jesli permission granted
      this._pickImage();
      console.log('permission granted');
    } else {
      console.log('permission not granted');
    }
    };

    _checkPermissions2 = () => {
      return new Promise((res, rej) => {
        Permissions.askAsync(Expo.Permissions.CAMERA_ROLL)
        .then((data) => {
          return res(data)
        })
        .catch((err) => {
          return rej(err)
        })
      })
    }

    _checkPermissions3 = () => {
      return new Promise((res, rej) => {
        Permissions.askAsync(Expo.Permissions.CAMERA)
        .then((data) => {
          return res(data)
        })
        .catch((err) => {
          return rej(err)
        })
      })
    }

    test = () => {
      this._checkPermissions3()
      .then((data) => this._pickImage())
      .catch(err => console.log(err))
    }

  _pickImage = async () => {
    console.log('test');
    let result = await ImagePicker.launchImageLibraryAsync({
      allowsEditing: true,
      aspect: [4, 3],
    });
    console.log(result);

    if (!result.cancelled) {
      this.setState({ image: result.uri });
    }
  };

  render() {
    let { image } = this.state;
    return (
      <View
        style={styles.container}
        collapsable={false}
        ref={view => {
          this._container = view;
        }}>
        <Text style={styles.paragraph}>
          Change code in the editor and watch it change on your phone!
          Save to get a shareable url. You get a new url each time you save.
        </Text>

        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
          <Button
            title="Pick an image from camera roll"
            onPress={this.test}
          />
          {image &&
            <Image source={{ uri: image }} style={{ width: 200, height: 200 }} />}
        </View>


        {this.state.cameraRollUri &&
          <Image
            source={{ uri: this.state.cameraRollUri }}
            style={{ width: 200, height: 200 }}
          />}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: Constants.statusBarHeight,
    backgroundColor: '#ecf0f1',
  },
  paragraph: {
    margin: 24,
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center',
    color: '#34495e',
  },
});
