import React from 'react';
import { CameraRoll, Image, ScrollView, StyleSheet, Text, View, TextInput, TouchableHighlight } from 'react-native';
import { Constants, ImagePicker } from 'expo';


import Container from '../components/Container';

export default class SignIn extends React.Component {

    state = {
      photos: null,
      image: '../assets/images/sign_in_username.png'
     };

     _pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      allowsEditing: true,
      aspect: [4, 3],
    });

    console.log(result);

    if (!result.cancelled) {
      this.setState({ image: result.uri });
    }
  };

    render() {
      let { photos, image } = this.state;
      console.log(image);
        return (
          <Container >

            <View style={styles.container}>
              <Text style={styles.header}>Sign in</Text>

              <TouchableHighlight
                  style={styles.addImage}
                  onPress={this._pickImage}
                  underlayColor='#fff'>
                  <View style={styles.avatar}>
                      <Image source={{uri: image}} style={{width: '100%', height: '100%'}} />
                  </View>
              </TouchableHighlight>

              {/*
              <View style={styles.logo}>
                  <Image source={require('../assets/images/sign_in_username.png')} style={{width: '100%', height: '100%'}} />
              </View>
              */}

              <TextInput
                style={styles.input}
                placeholder="Username"
                onChangeText={(text) => this.setState({text})}
              />

              <TextInput
                style={styles.input}
                placeholder="Email address"
                onChangeText={(text) => this.setState({text})}
              />

              <TextInput
                style={styles.input}
                placeholder="PL Polska +48"
                onChangeText={(text) => this.setState({text})}
              />

              <TextInput
                style={styles.input}
                placeholder="+48 Your phone number"
                onChangeText={(text) => this.setState({text})}
              />

            </View>

          </Container>
        );
    }



}

const styles = StyleSheet.create({
  contentContainer: {
    paddingVertical: 20
  },
    container: {
        justifyContent: 'center',
        marginTop: 75,
        alignItems: 'center',
        color: '#fff'
    },
    addImage: {

    },
    input: {
      backgroundColor: '#003853',
      textAlign: 'center',
      marginTop: 15,
      width: 250,
      borderRadius: 15,
      padding: 10
    },
    header: {
        textAlign: 'center',
        fontSize: 22,
        color: 'white',
        margin: 0

    },
    bckImage: {
        width: '100%'
    },
    avatar: {
        width: 150,
        height: 150,
        backgroundColor: 'rgba(0, 0, 0, 0)',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 35,
        borderRadius: 15

    },
    siginBtn: {
        backgroundColor: 'rgba(90, 17, 67, 0)',
        marginTop: 135
    },
    siginsigninTxt: {
        color: '#fff',
        marginTop: 35
    },
    siginsigninTxtPolicy: {
        color: '#fff',
        marginTop: 0,
        fontWeight: 'bold'
    },
    siginsigninTxtStart: {
        color: '#fff',
        marginTop: 20,
        marginBottom: 10,
        fontSize: 25
    },
    siginsigninBold: {
        color: '#fff',
        fontSize: 25,
        fontWeight: 'bold'
    },
    submit:{
        marginRight:40,
        marginLeft:40,
        marginTop:10,
        paddingTop:10,
        paddingBottom:10,
        backgroundColor:'#e72dac',
        borderRadius:20,
        borderWidth: 0,
        width: 150
    },
    submitText:{
        color:'#fff',
        textAlign:'center'
    }
});
