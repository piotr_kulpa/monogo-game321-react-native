/* @flow */

import { AnimatedCircularProgress } from 'react-native-circular-progress';
import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
} from 'react-native';

export default class Progress extends Component {
  render() {
    return (
      <View style={styles.container}>
        <AnimatedCircularProgress
          style={styles.circle}
          size={120}
          width={15}
          fill={100}
          tintColor="#00e0ff"
          onAnimationComplete={() => console.log('onAnimationComplete')}
          backgroundColor="#3d5875"
          duration={10000}
          rotation={0}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  circle: {

  }
});
