// TODO: test react-native-cli
import React from 'react';
import {View,  ImageBackground} from 'react-native';
import {createStackNavigator, createAppContainer} from "react-navigation";

//screens
import Home from './screens/Home';
import Profile from './screens/Profile';
import Leaderboards from './screens/Leaderboards';
import Invite from './screens/Invite';
import AboutUs from './screens/AboutUs';
import Faq from './screens/Faq';
import Contact from './screens/Contact';
import SignIn from './screens/SignIn';
import List from './screens/FlatListBasics';
import Privacy from './screens/Privacy';
import TestImagePicker from './screens/TestImagePicker';
import Notifications from './screens/Notifications';
import CheckPermissions from './screens/CheckPermissions';
import Geolocalization from './screens/Geolocalization';
import Progress from './screens/Progress';
import ModalExample from './screens/Modal';


//apollo, redux
import {ApolloClient, createNetworkInterface} from 'apollo-client';
import {ApolloProvider} from "react-apollo";
import { HttpLink } from 'apollo-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import reducers from './reducers';

//configure Apollo client
const config = {
    link: new HttpLink({ uri: 'http://10.0.2.2:5678/graphql' }),
    cache: new InMemoryCache(),
};

//new Apollo client
const client = new ApolloClient(config);

//pass thunk as middleware
const store = createStore(reducers, applyMiddleware(thunk))

//screen register
const AppNavigator = createStackNavigator({
    Home: {screen: Home},
    Profile: {screen: Profile},
    Leaderboards: {screen: Leaderboards},
    Invite: {screen: Invite},
    AboutUs: {screen: AboutUs},
    Faq: {screen: Faq},
    Contact: {screen: Contact},
    SignIn: {screen: SignIn},
    List: {screen: List},
    Privacy: {screen: Privacy},
    Geolocalization: {screen: Geolocalization},
    Progress: {screen: Progress},
    ModalExample: {screen: ModalExample}
},
{
    headerMode: 'none'
  }
);

//create main React Native screen
const AppContainer = createAppContainer(AppNavigator);


//wrappers Redux -> Apollo -> App
export default class App extends React.Component {
    render() {
        return (
            <Provider store={store}>
                <ApolloProvider client={client}>




                            <AppContainer />



                </ApolloProvider>
            </Provider>
        );
    }
}
