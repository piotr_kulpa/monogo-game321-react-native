import React, { Component } from 'react';
import { AppRegistry, View, StyleSheet, Text, Button, Image, TouchableHighlight } from 'react-native';
// TODO: add action to hamburger

export default class Header extends Component {



    render() {
        return (
            // Try setting `flexDirection` to `column`.
            <View style={styles.header}>

                <TouchableHighlight
                  style={{flex: 1}}

                >
                  <Text style={{flex: 1}}></Text>

                </TouchableHighlight>

                <View style={{flex: 1}} ><Image style={{width: 180, height: 50, alignSelf: 'center'}} source={require('../assets/images/logo_top.png')} /></View>
                <View style={{flex: 1}} ><Image style={{width: 50, height: 50, alignSelf: 'flex-end'}} source={require('../assets/images/person_top.png')} /></View>
            </View>
        );
    }
};

const styles = StyleSheet.create({
    header: {
        marginTop: 0,
        alignItems: 'center',
        justifyContent: 'center',
        position: 'relative',
        zIndex: 1,
        flex: 1,
        flexDirection: 'row'
    }
});
