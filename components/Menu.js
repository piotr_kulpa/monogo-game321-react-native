/* @flow */

import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Button,
  TouchableHighlight,
  Image
} from 'react-native';
import { withNavigation } from 'react-navigation';
import { connect } from 'react-redux';

class Menu extends Component {


  render() {

    return (
      <View style={styles.container}>


        <View style={styles.hamburgerBtnWrapper}>
          <TouchableHighlight
            style={styles.hamburgerBtn}
            onPress={() => this.props.navigation.navigate('Profile')}
          >
            <Text style={styles.hamburgerBtnTxt}>PROFILE</Text>


          </TouchableHighlight>

          <TouchableHighlight
            style={styles.hamburgerBtn}
            onPress={() => this.props.navigation.navigate('Leaderboards')}
          >
            <Text style={styles.hamburgerBtnTxt}>LEADERBOARD</Text>
          </TouchableHighlight>

          <TouchableHighlight
            style={styles.hamburgerBtn}
            onPress={() => this.props.navigation.navigate('Invite')}
          >
            <Text style={styles.hamburgerBtnTxt}>INVITE</Text>
          </TouchableHighlight>

          <TouchableHighlight
            style={styles.hamburgerBtn}
            onPress={() => this.props.navigation.navigate('AboutUs')}
          >
            <Text style={styles.hamburgerBtnTxt}>ABOUT US</Text>
          </TouchableHighlight>

          <TouchableHighlight
            style={styles.hamburgerBtn}
            onPress={() => this.props.navigation.navigate('List')}
          >
            <Text style={styles.hamburgerBtnTxt}>List</Text>
          </TouchableHighlight>

          <TouchableHighlight
            style={styles.hamburgerBtn}
            onPress={() => this.props.navigation.navigate('Privacy')}
          >
            <Text style={styles.hamburgerBtnTxt}>PRIVACY</Text>
          </TouchableHighlight>

          <TouchableHighlight
            style={styles.hamburgerBtn}
            onPress={() => this.props.navigation.navigate('Faq')}
          >
            <Text style={styles.hamburgerBtnTxt}>FAQ</Text>
          </TouchableHighlight>

          <TouchableHighlight
            style={styles.hamburgerBtn}
            onPress={() => this.props.navigation.navigate('Geolocalization')}
          >
            <Text style={styles.hamburgerBtnTxt}>Geolocalization</Text>
          </TouchableHighlight>

          <TouchableHighlight
            style={styles.hamburgerBtn}
            onPress={() => this.props.navigation.navigate('Progress')}
          >
            <Text style={styles.hamburgerBtnTxt}>Progress</Text>
          </TouchableHighlight>

          <TouchableHighlight
            style={styles.hamburgerBtn}
            onPress={() => this.props.navigation.navigate('ModalExample')}
          >
            <Text style={styles.hamburgerBtnTxt}>Modal</Text>
          </TouchableHighlight>
        </View>



      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: 250,
    backgroundColor: '#282828'

  },
  hamburgerBtnWrapper: {
      textAlign: 'right',
      marginTop: 80
  },
  hamburgerBtnTxt: {
      textAlign: 'right'
  }
});

const mapStateToProps = state => {

  return {
    toggle: state.toggleMenu
  }
}

export default withNavigation(connect(mapStateToProps)(Menu));
