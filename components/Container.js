/* @flow */

import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  ImageBackground
} from 'react-native';
import Menu from './Menu';
import Footer from './Footer';
import Header from './Header';
import Hamburger from './Hamburger';

export default class Container extends Component {
  render() {

    return (
      <View style={styles.container}>
        <ImageBackground source={require('../assets/images/sign_in.jpg')} style={{width: '100%', height: '100%'}}>
          <Hamburger style={styles.hamburger}/>
          <Header style={styles.header} />
          <View style={styles.content}>{this.props.children}</View>
          <Footer style={styles.footer} />
        </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
  },
  hamburger: {
    zIndex: 99
  },
  header: {
    flex: 1
  },
  footer: {
    flex: 1
  },
  content: {
    flex: 4
  }
});
