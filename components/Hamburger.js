// TODO: set action
import React, { Component } from 'react';
import {AppRegistry, View, StyleSheet, Text, TouchableHighlight, Image, Button, Animated} from 'react-native';
import toggleMenu from '../actions/toggleMenu.js';
import { connect } from 'react-redux';
import Menu from './Menu';

class Hamburger extends Component {

  config = {
    animSpeed: 600,
    menuWidth: 250
  }

  state = {
    isOpen: false,
    xAnim: new Animated.Value(-this.config.menuWidth)
  }

  toggleMenu() {
    switch(this.state.isOpen) {
      case false:
        this.setState({isOpen: true});
        this.showMenu();
        break;
      case true:
        this.setState({isOpen: false})
        this.hideMenu();
        break;
    }

    console.log(this.state.isOpen);
  }

  showMenu() {
    Animated.timing(                  // Animate over time
     this.state.xAnim,            // The animated value to drive
     {
       toValue: 0,                   // Animate to opacity: 1 (opaque)
       duration: this.config.animSpeed,              // Make it take a while
     }
   ).start();
  }

  hideMenu() {
    Animated.timing(                  // Animate over time
     this.state.xAnim,            // The animated value to drive
     {
       toValue: -this.config.menuWidth,                   // Animate to opacity: 1 (opaque)
       duration: this.config.animSpeed,              // Make it take a while
     }
   ).start();
  }


  render() {
    let { xAnim } = this.state;
    return (
      // Try setting `flexDirection` to `column`.
      <View style={styles.hamburgerWrapper} >
        <TouchableHighlight
          style={styles.hamburger}
          onPress={() => this.toggleMenu()}
        >
          <Image
              style={{width: 50, height: 50}}
              source={require('../assets/images/hamburger.png')}
            />
        </TouchableHighlight>

        <Animated.View
          style={{
            ...this.props.style,
            position: 'relative',
            zIndex: 97,
            left: xAnim
          }}
        >
          <Menu />
        </Animated.View>
      </View>
    );
  }
};

const styles = StyleSheet.create({
    hamburgerWrapper: {
        zIndex: 99,
        backgroundColor: 'rgba(0, 0, 0, 0)',
        position: 'absolute'
    },
    hamburger: {
        zIndex: 2,
        marginTop: 30,
        height: 50,
        position: 'absolute',
        zIndex: 98
    },
    hamburgerBtnTxt: {
        textAlign: 'right'
    }
});

const mapStateToProps = state => {

  return {
    toggle: state.toggleMenu
  }
}

export default connect(mapStateToProps, {toggleMenu})(Hamburger);
