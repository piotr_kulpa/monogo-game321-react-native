import React, {Component} from 'react';
import {View, StyleSheet, Text, Button, TouchableHighlight} from 'react-native';
import { withNavigation } from 'react-navigation';

class Footer extends Component {
    render() {
        return (

                <View style={styles.footer}>
                    <TouchableHighlight
                      style={[styles.footerBtn, {backgroundColor: "#14a2c8"}]}
                      onPress={() => this.props.navigation.navigate('Leaderboards')}
                    >
                        <Text style={styles.footerBtnTxt}>LEADERBOARD</Text>
                    </TouchableHighlight>
                    <TouchableHighlight
                      style={styles.footerBtn}
                      onPress={() => this.props.navigation.navigate('Invite')}
                      >
                        <Text style={styles.footerBtnTxt}>INVITE</Text>
                    </TouchableHighlight>

                </View>

        );
    }
}

const styles = StyleSheet.create({
    footer: {
        flex: 1,
        flexDirection: "row",
        alignItems: "center"
    },
    footerBtn: {
        marginRight:40,
        marginLeft:40,
        marginTop:10,
        paddingTop:10,
        paddingBottom:10,
        backgroundColor:'#e72dac',
        borderRadius:20,
        borderWidth: 0,
        width: 150,
        flex: 1
    },
    footerBtnTxt: {
        color: "white",
        textAlign: "center"
    }
});

export default withNavigation(Footer);
