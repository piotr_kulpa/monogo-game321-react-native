# Project Title

321!GAME React Native test enviroment

## Getting Started

* clone project
* install packages npm install
* cd project folder and run npm start

### Installing

* npm install

End with an example of getting some data out of the system or using it for a little demo

## Git naming conventions

```
GAME321-<jira task number> <git styles> <comment>
```

Git styles:
* feat: a new feature
* fix: a bug fix
* docs: changes to documentation
* style: formatting, missing semi colons, etc; no code change
* refactor: refactoring production code
* test: adding tests, refactoring test; no production code change
* chore: updating build tasks, package manager configs, etc; no production code change

### Deployment

Run:
```
expo build:android
```

or

```
expo build:ios
```

## License

This project is licensed under the ... License.
