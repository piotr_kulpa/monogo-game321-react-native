import gql from 'graphql-tag';

export const faqQuery = gql`
query privacyQuery{
  Game321QuizGameApiStaticContentGetStaticContent{
    faq{
      title
      content{
        header
        text
      }
    }
  }
}
`;

export default faqQuery;
