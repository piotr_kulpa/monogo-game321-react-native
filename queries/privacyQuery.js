import gql from 'graphql-tag';

export const privacyQuery = gql`
query privacyQuery{
  Game321QuizGameApiStaticContentGetStaticContent{
    privacy{
      title
      content{
        header
        subheader
        text
      }
    }
  }
}
`;

export default privacyQuery;
