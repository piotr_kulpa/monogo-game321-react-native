import { combineReducers } from 'redux';
import toggleMenuReducer from './toggleMenu.js';



// register reducers
export default combineReducers({
    toggleMenu: toggleMenuReducer
});
